Vagrant.configure("2") do |config|
  config.vbguest.auto_update = false
  IMAGE_NAME="bento/ubuntu-18.04"
  MEMORY_MASTER=2048
  MEMORY_NODE=512

  config.vm.define "k8s-master" do |master|
    master.vm.box = IMAGE_NAME
    master.vm.network "private_network", ip: "192.168.50.10"
    master.vm.hostname = "k8s-master"

    master.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--memory", MEMORY_MASTER]
      v.customize ["modifyvm", :id, "--name", "k8s-master"]
    end

    master.vm.provision "ansible" do |ansible|
      ansible.playbook = "kubernetes-setup/master-playbook.yml"
      ansible.extra_vars = {
        node_ip: "192.168.50.10",
      }
    end
  end

  servers=[
    {
      :hostname => "node01",
      :box => IMAGE_NAME,
      :ip => "192.168.50.11",
      :ssh_port => '2001'
    }
  ]

  servers.each do |machine|

    config.vm.define machine[:hostname] do |node|
      node.vm.box = machine[:box]
      node.vm.hostname = machine[:hostname]
    
      node.vm.network :private_network, ip: machine[:ip]
      node.vm.network "forwarded_port", guest: 22, host: machine[:ssh_port], id: "ssh"

      node.vm.provider :virtualbox do |v|
        v.customize ["modifyvm", :id, "--memory", MEMORY_NODE]
        v.customize ["modifyvm", :id, "--name", machine[:hostname]]
      end

      node.vm.provision "ansible" do |ansible|
        ansible.playbook = "kubernetes-setup/node-playbook.yml"
        ask_sudo_pass = false
        ansible.extra_vars = {
          node_ip: machine[:ip],
        }
      end
    end
  end

end